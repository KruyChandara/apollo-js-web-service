# Description

This is a web service that provide Graph API to access KIT's information. Build by `Apollo JS` and `Postrgesql` Database.

# Set Up

1.`npm install`
2.`npm start `

After running start script the server will run on http://localhost:4000.
For testing your Graph API navigate to: http://localhost:4000/graphql