require("dotenv").config();

var pg = require("knex")({
  client: "pg",
  connection: {
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE
  }
});

console.log(process.env.PASSWORD);
module.exports = pg;
