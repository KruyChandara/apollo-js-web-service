const { gql } = require("apollo-server");

const typeDefs = gql`
  type Student {
    id: ID!
    name: String
    batch: String
  }
  type Query {
    students: [Student]!
    student(id: ID!): Student
  }
`;

module.exports = typeDefs;
