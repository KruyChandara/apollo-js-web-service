const { DataSource } = require("apollo-datasource");

class Student extends DataSource {
  constructor({ pg }) {
    super();
    this.pg = pg;
  }

  /**
   * This is a function that gets called by ApolloServer when being setup.
   * This function gets called with the datasource config including things
   * like caches and context. We'll assign this.context to the request context
   * here, so we can know about the user making requests
   */
  initialize(config) {
    this.context = config.context;
  }

  async getAllStudents() {
    const response = await this.pg
      .limit(10)
      .select()
      .table("op_student");
    return Array.isArray(response)
      ? response.map(student => this.studentReducer(student))
      : [];
  }

  studentReducer(student) {
    return {
      name: student.last_name,
      id: student.id,
      batch: student.batch_id
    };
  }
}

module.exports = Student;
