module.exports = {
  Query: {
    students: (_, __, { dataSources }) => dataSources.student.getAllStudents(),
    student: (_, { id }, { dataSources }) =>
      dataSources.student.getStudentById({ studentID: id })
  }
};
