const { ApolloServer } = require("apollo-server");
const typeDefs = require("./schema");
const pg = require("./utils");
const resolvers = require("./resolvers");

const Student = require("./datasources/student");

require("dotenv").config();

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({
    student: new Student({ pg })
  })
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
